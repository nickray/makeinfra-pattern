# MakeInfra pattern #

This repository introduces the `MakeInfra` pattern [briefly discussed on a blog I run](https://vadosware.io/post/using-makefiles-and-envsubst-as-an-alternative-to-helm-and-ksonnet/).

It will serve as documentation of the pattern as well as a compendium of approaches/use-patterns for handling various concerns, as they are discovered/standardized. While this pattern doesn't have a spec, proper RFC process, or published standard, this repo will serve as the precursor to those other things.

This repository serves as what a `-infra` repo might look like for a time management/TODO application, called `todo`. Note that while this repo is specific to one app (the project might be named `todo-infra` if it were real), these concepts can be applied at a higher level (see Project/Infrastrucure nesting section below).

# Demo #

To demo this repository:

0. Set up a [kubernetes](https://kubernetes.io) cluster, and correctly configure `kubectl` to access it, ensuring you do not have a namespace named `todo` present
1. `git clone <this repo>`
2. `cd makeinfra-pattern`
3. `make`

This should deploy fake requirements for a todo application, including a caching (`redis` deployment) and persistence (`postgres` deployment) layer along with a fake frontend (`nginx` deployment), using secrets as it does so.

Here are what the resources look lik when started properly:
```
$ k get all -n todo
NAME                         DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
deploy/todo-postgres-v10.4   1         1         1            1           15m
deploy/todo-redis-v4.0       1         1         1            1           15m
deploy/todo-v1               1         1         1            1           52s

NAME                               DESIRED   CURRENT   READY     AGE
rs/todo-postgres-v10.4-fd58dc6d4   1         1         1         9m
rs/todo-redis-v4.0-745d786c9d      1         1         1         15m
rs/todo-v1-687c67d6b               1         1         1         52s

NAME                                     READY     STATUS    RESTARTS   AGE
po/todo-postgres-v10.4-fd58dc6d4-bht46   1/1       Running   0          9m
po/todo-redis-v4.0-745d786c9d-c9jcp      1/1       Running   0          15m
po/todo-v1-687c67d6b-z895q               1/1       Running   0          52s

NAME                TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
svc/todo-postgres   ClusterIP   10.110.238.139   <none>        5432/TCP   15m
svc/todo-redis      ClusterIP   10.108.10.17     <none>        5432/TCP   15m
svc/todo-v1         ClusterIP   10.96.147.201    <none>        5432/TCP   19s
```

To tear everything down you can run `make uninstall`

# Context #

The `MakeInfra` pattern is an attempt to merge concepts that have been gaining traction lately: a focus on release/operations engineering ("DevOps"), and a focus on declarative (and if possible immutable) infrastructure. After creating a piece of software, it's important to consider how it's going to be deployed, and my personal favorite (at this point) is [Kubernetes](https://https://kubernetes.io/docs/concepts). Kubernetes manages a group of nodes as a heterogeneous pool of resources, and relies on the user writing and creating (or "applying") declarative configurations of those resources in order to run "workloads". For example, to run [Redis](https://redis.io/), you might write a configuration for a resource that looks like this:  "resources"

# What is the MakeInfra/MakeOps pattern? #

While I thought up the `MakeInfra` pattern as a reaction to [Helm](https://docs.helm.sh) (which is a kubernetes-specific tool), it can certainly apply to any other similar declaratively (or even iteratively) provisioned resources. You could certainly use this pattern to run ansible -- It's just `Makefile`s and some naming conventions/associated tooling. Go wild with it.

The core ideas of the `MakeInfra` pattern are simple:

- **For every `<project>` repo, make a corresponding `<project>-infra` repository** - this becomes composable by using `git submodule` for something like `<team>-infra`, or even a [monorepo][monorepo] for infra
- **Name resource definitions with descriptive suffixes** - i.e. `jaeger.deployment.yaml`, `monitoring.ns.yaml`
- **Use a `Makefile` with `.PHONY` targets** in the folder with your resource definitions
- **Use [`git-crypt`](https://www.agwa.name/projects/git-crypt/) to store credentials inside the `-infra` repo** - for testing, staging, and maybe even production, right in the repo.
- **Suffix files that must be transformed with `.pre`** - This makes it pretty clear to anyone which files are ready and which are not -- you can also use whatever tool you want (KSonnet, `envsubst`, `m4`, `python`) for replacement.
- **Use the tools that make sense for your team** - just like you would for any other `Makefile` powered project (so technically, you can use `helm`, or `jinja2`, or raw `perl`, or `ksonnet` whatever else)

**I think this idea is more powerful than tools helm because it's a superset of the ideas behind helm (as in you can use helm very easily with this approach), and it decomposes well enough to offer a subset of the features offered by a tool like helm when you're starting o\
ut.**

When you want to ensure the project is up, if your local `kubectl` is already properly configured, you only need to clone the repo, enter, and run `make` (or optionally `make deploy` or some other more specific target)!

# How does `MakeInfra` handle... #

## File transformation/Pre-processing ##

Rather than hard-coding various dynamic variables (e.g. server to aim at, version of software to deploy) into files, it's obvious to allow for some small amount of dynamism when configuring your infrastructure.

**The `MakeInfra` pattern suggests that you use [`envsubst`](https://www.gnu.org/software/gettext/manual/html_node/envsubst-Invocation.html) (or another similar) tool to solve this problem. In addition to using a software like `envsubst`, it is suggested that you use a consistent naming pattern, like suffixing all files that require building with `.pre` to indicate that they must be built**. The idea here is to make it very obvious when you use files that they must be run through some sort of transformation tool first. It is important to also include the necessary `Makefile` targets to ensure the user doesn't need to know too much about **how** the file is built, but just that they can run something like `make file.extension` and the file will get made as it should.

As mentioned earlier, you are free to use another simliar tool, or full blown programming language to solve this problem, it just happens that `envsubst` is a relatively simple tool, and if simple substitution is largely your usecase, it should be very beneficial.

See: The `todo` app folder itself provides an example. You can try it yourself in the command line by running the following:

```
$ make todo/v1.0/deployment.yaml # alternatively, `cat todo/v1.0/todo.deployment.yaml.pre | REPLICA_COUNT=3 envsubst > todo.deployment.yaml`
```

## Secrets ##

Where to store secrets in your infrastructure is a question that can be frought with much worry and uncertainty. Depending on where you are deployed, the answer can be different as well -- should you use [Hashicorp Vault](https://www.vaultproject.io/) (probably), or [AWS Parameter Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-paramstore.html)? How should access be provided? How will deployment robots access the repo? How about developers -- what happens when someone quits?

**The `MakeInfra` pattern suggests that you use [git-crypt](https://www.agwa.name/projects/git-crypt/) (or another similar tool) to solve this problem**. There are a myriad of options, and you could certainly use a tool like Vault with the `MakeInfra` approach, but an emphasis on simplicity suggests that the easiest way to solve the problem would be to store the credentials next to the other infrastructure code we've now made space for. The only pesky thing left to solve is to securely store the information -- a problem which programs like `git-crypt` provide a solution to.

In this repo there is a `secrets` folder that contains `git-crypt` encrypted secrets for various environments. The files are encrypted with

**Ideally**, keys/credentials should be rotated any time someone who had access to them leaves the company (and in a perfect world this would be done automatically), but I consider operational security aspects like that beyond the scope of this document. All the `MakeInfra` pattern provides here is a **suggestion** to use `git-crypt`, since it is simple and safe assuming you use GPG (or a shared secret) with proper operational security.

See: The `todo/v1.0/persistence/v1.0/postgres/v10.4/secrets` folder is a great example of `git-crypt` in action (don't forget to check out the `.gitattributes` file, of course).

The `git-crypt` key file has been included in the repo, so feel free to [read through the git-crypt documentation](https://www.agwa.name/projects/git-crypt/) to learn how to use it. The process should be something like:

```
$ <install git-crypt>
$ git clone <this repo>
$ cd makeinfra-pattern
$ git-crypt unlock git-crypt.key
```

## Project/Infrastructure nesting ##

**The `MakeInfra` pattern suggests that you break apart your complicated infrastructure using nested `make` invocations**. Using nested `make` invocations enables different parts of the infrastructure to be split out into their own folders, reducing cognitive load, and hopefully allowing for more precise changes.

See: The `todo/v1.0/cache` folder is a good example of this.

At a much higher level, the `MakeInfra` structure can be applied at the team or organization by using [`git submodule`](https://git-scm.com/docs/git-submodule) and the concept of a [Monorepo](https://danluu.com/monorepo/)

**The `MakeInfra` pattern suggests that you use `git submodule` (if necessary) to infrastructure at higher levels of grouping (e.g. teams or a whole organization).** For example if you choose to have an `ba-infra` repo for the Business Analytics team, it likely makes sense to include any projects the BA team owns in the repo, but as git submodules. With a `Makefile` at this level, a single `make` could deploy every piece of software necessary for the team to function.

## Versioning/Rollback ##

As more and more software is deployed, the importance of versioning is obvious. Knowing which versions of the software contained specific defects and being able to deploy different specific versions is critical to sane operations.

**The `MakeInfra` pattern suggests that you version applications/components in/of your infrastructure with a simple folder scheme**. This means that if you deploy a very specific application (with let's say `make app`), you should make it possible to pass along a `VERSION` make variable (making the whole command `make app VERSION=1.0` for example). While there are lots of ways you could implement this, the simplest might be to rely on a folder structure that mimics the application/component, so something like `app/v1.0/...`.

Note that combining this concept with infrastructure component nesting means that version resolution at higher levels must be either hardcoded, or care must be taken when passing in versions (a single `VERSION` would likely not suffice). While care must be taken when managing this complexity, the simplest case is still quite simple. By simply versioning the multiple levels of dependencies, it is trivial to resolve versions statically.

In addition to adopting the simplest versioning scheme (static, hardcoded versions hierarchy), if your infrastructure is sufficiently declarative (and/or immutable), you should be able to achieve rollback capability by simply never deleting any subdirectories, opting instead to copy and modify when versions change.

See: The `todo/v1.0/cache/v1.0/redis` sub-component is a good example of this. It's a bit inelegant but very very explicit.

## BONUS: CI integration ##

With a flexible enough CI system (like [Gitlab's excellent CI system](https://docs.gitlab.com/ee/ci/)), it's fairly easy to integrate web-triggered and automatic deploys (since "deploying" is just running `make`). I'll leave that as an exercise for the reader.

# Warts/Issues #

## Caching done by Make ##

`make` will cache files when they've been generated, so ensure to add the appropriate cache busting code, and don't get caught by a stale file or two.

# Contributing #

If you see some bit of this pattern that could be streamlined or improved, or simply want to ask a question or start a discussion, Merge requests are welcomed!
